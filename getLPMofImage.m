function LPM = getLPMofImage(image_name, lpm_folder, features)

% 11 segment label types for each Helen image:
% 
%    Label 00: background
%    Label 01: face_skin (excluding ears and neck)
%    Label 02: left_eyebrow
%    Label 03: right_eyebrow
%    Label 04: left_eye
%    Label 05: right_eye
%    Label 06: nose
%    Label 07: upper_lip
%    Label 08: inner_mouth
%    Label 09: lower_lip
%    Label 10: hair

lpms_folder = fullfile(lpm_folder, strrep(image_name, '.jpg', ''));

regions = { 'background', 'face_skin', 'left_eyebrow', 'right_eyebrow', 'left_eye', ...
            'right_eye', 'nose', 'upper_lip', 'inner_mouth', 'lower_lip', 'hair'};

n_ftrs2retrieve = numel(features);

for nf=1:n_ftrs2retrieve
    ind = find(~cellfun('isempty', strfind(regions, features{nf}))) - 1;
    if(isempty(ind))
        eval(['LPM.' features{nf} '=''feature not found'';']);
        continue;
    end
    name_lpm = strrep(image_name, '.jpg', ['_lbl' sprintf('%02d', ind) '.png']);
    lpm_file = fullfile(lpms_folder, name_lpm);
    eval(['LPM.' features{nf} '=imread(lpm_file);']);

    % % Global label priority map
    if (nf==1)
        eval(['LPM.global = LPM.' features{nf} ';']);
    else
        eval(['LPM.global = LPM.global + LPM.' features{nf} ';']);
    end

end