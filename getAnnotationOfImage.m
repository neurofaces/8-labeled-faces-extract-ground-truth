function annotation = getAnnotationOfImage(image_name, annotations_folder)

% Annotations inside annotations_folder are stored in txt files with
% numbers from 1 to 2330 as names.
% First step is to match each annotation txt file with its corresponding
% image.
annotation_files = getImageAnnotationCorrespondeces(annotations_folder);

% Afterwards corresponding file is loaded and landmarks are stored in
% annotation
annotation_file_ind = strfind(annotation_files, strrep(image_name, '.jpg', ''));
annotation_file_ind = find(~cellfun('isempty', annotation_file_ind));
annotation_file = fullfile(annotations_folder, [num2str(annotation_file_ind) '.txt']);
annotation = load_annotations_file(annotation_file);
