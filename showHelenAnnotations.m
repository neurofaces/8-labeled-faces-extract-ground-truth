% function lpm = getFeatureLPMofImage(image_name, feature_name)

function showHelenAnnotations()

% % Get test images and their annotation files

% % Images fodler
test_images_folder = '../_common_dbs/helen-test/';
% % LPMs folder
annotations_folder = '../_common_dbs/helen-annotation/';

% % Load image, get annotations
test_images = dir([test_images_folder '*.jpg']);
n_test_images = numel(test_images);
for ni=1:n_test_images
    
    image_name = test_images(ni).name;
    image = imread(fullfile(test_images_folder, image_name));
    
    annotation = getAnnotationOfImage(image_name, annotations_folder);
    
    image2=rgb2gray(image);
    try
        image2(sub2ind(size(image2), round(annotation(:,2)), round(annotation(:,1)))) = 255;
        figure; imshow(image2);
    catch e
        disp(e.getReport());
    end
end