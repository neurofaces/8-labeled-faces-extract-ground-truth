function annotation_image_correspondence = getImageAnnotationCorrespondeces(annotations_folder)

% % Match annotation file with image filename
annotation_files = dir([annotations_folder '*.txt']);
n_annotation_files = numel(annotation_files);
annotation_image_correspondence = cell(n_annotation_files, 1);
for naf=1:n_annotation_files
    fp = fopen(fullfile(annotations_folder, annotation_files(naf).name));
    aic_ind = str2double(strrep(annotation_files(naf).name, '.txt', ''));
    annotation_image_correspondence{aic_ind} = fgetl(fp); % image name is in first line of annotation file
    fclose(fp);
end