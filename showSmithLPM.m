function showSmithLPM()

% We provide 11 segment label types for each Helen image:
% 
%    Label 00: background
%    Label 01: face skin (excluding ears and neck)
%    Label 02: left eyebrow
%    Label 03: right eyebrow
%    Label 04: left eye
%    Label 05: right eye
%    Label 06: nose
%    Label 07: upper lip
%    Label 08: inner mouth
%    Label 09: lower lip
%    Label 10: hair

% % Get test images and their annotation files

% % Images fodler
test_images_folder = '../_common_dbs/helen-test/';
% % LPMs folder
lpm_folder = '../_common_dbs/smith-labels';

% % Load image and show LPM
test_images = dir([test_images_folder '*.jpg']);
n_test_images = numel(test_images);
for ni=1:n_test_images

    image_name = test_images(ni).name;
    image = imread(fullfile(test_images_folder, image_name));

    LPM = getLPMofImage(image_name, lpm_folder, {'left_eyebrow', 'right_eyebrow'});

    bounds = bwboundaries(LPM.global);
    boundaries = cellfun(@reorderVertices, bounds, 'UniformOutput', false);
    image2 = insertShape(image, 'Polygon', {[boundaries{1,1}], [boundaries{2,1}]}, 'Color', {'red'}, 'Opacity', 1);

    figure; imshow(image2);
    pause;
    close all;
end


function vertices=reorderVertices(boundaries)
    x = boundaries(:,2);
    y = boundaries(:,1);
    nl = numel(x)*2;
    
    vertices = zeros(1, nl);
    vertices(1:2:end) = x;
    vertices(2:2:end) = y;